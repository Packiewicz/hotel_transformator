use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead, BufReader},
};

use csv::Error;
use serde::{Deserialize, Serialize};

mod date;

#[derive(Debug, Deserialize)]
struct InputRecord {
    city_code: String,
    hotel_code: String,
    room_type: String,
    room_code: String,
    meal: String,
    checkin: String,
    adults: i32,
    children: i32,
    price: f32,
    source: String,
}

#[derive(Debug, Deserialize, Clone)]
struct HotelRecord {
    id: String,
    city_code: String,
    name: String,
    category: f32,
    country_code: String,
    city: String,
}

#[derive(Deserialize, Debug, Clone, Hash, Eq, PartialEq)]
struct RoomHashKey {
    hotel_code: String,
    source: String,
    room_code: String,
}

#[derive(Debug, Deserialize, Clone)]
struct RoomRecord {
    hotel_code: String,
    source: String,
    room_name: String,
    room_code: String,
}


#[derive(Debug, Deserialize, Serialize)]
struct OutputRecord {
    #[serde(rename(serialize = "room_type meal"))]
    room_type_meal: String,
    room_code: String,
    source: String,
    hotel_name: String,
    city_name: String,
    city_code: String,
    hotel_category: f32,
    pax: i32,
    adults: i32,
    children: i32,
    room_name: String,
    checkin: String,
    checkout: String,
    price: String,
}

type RoomStorage = HashMap<RoomHashKey, String>;

fn concat_strings(lhs: &str, rhs: &str) -> String {
    let mut full_path = lhs.to_owned();
    full_path.push_str(rhs);
    full_path
}

fn process_input(base_path: &str) -> Result<(), Error> {
    let mut reader = csv::ReaderBuilder::new()
        .delimiter(b'|')
        .from_path(concat_strings(base_path, "input.csv"))?;
    let mut writer = csv::WriterBuilder::new()
        .delimiter(b';')
        .from_path(concat_strings(base_path, "output.csv"))?;
    let hotels = read_hotels(&concat_strings(base_path, "hotels.json"))?;
    let rooms = read_rooms(&concat_strings(base_path, "room_names.csv"))?;

    println!("{:?}", rooms);

    for result in reader.deserialize::<InputRecord>() {
        let record = result?;

        let hotel_record = match hotels.get(&record.hotel_code) {
            Some(h) => h,
            None => panic!("No matching hotel for id: {}", record.hotel_code),
        };

        let room_key = RoomHashKey {
            hotel_code: record.hotel_code,
            source: record.source.clone(),
            room_code: record.room_code.clone()
        };
        let room_name = match rooms.get(&room_key) {
            Some(r) => r,
            None => panic!("No matching room for key {:?}", room_key),
        };

        let checkin_date = date::Date::from_str(&record.checkin);
        if checkin_date.is_none() {
            panic!("Couldn't parse date: {}", &record.checkin)
        }
        let checkin_date = checkin_date.unwrap();

        let mut day_after_checkin = checkin_date.clone();
        day_after_checkin.add_day();

        let price_per_person = record.price / ((record.adults + record.children) as f32);
        println!("{:.6}", price_per_person);
        println!("{:.2}", price_per_person);
        let price_per_person = format!("{:.2}", price_per_person);

        let output_record = OutputRecord {
            room_type_meal: format!("{} {}", record.room_type, record.meal),
            room_code: record.room_code,
            source: record.source,
            hotel_name: hotel_record.name.clone(),
            city_name: hotel_record.city.clone(),
            city_code: hotel_record.city_code.clone(),
            hotel_category: hotel_record.category,
            pax: record.adults + record.children,
            adults: record.adults,
            children: record.children,
            room_name: room_name.clone(),
            checkin: checkin_date.to_string(),
            checkout: day_after_checkin.to_string(),
            price: price_per_person
        };
        writer.serialize(output_record)?;
    }
    Ok(())
}

fn read_hotels(path: &str) -> io::Result<HashMap<String, HotelRecord>> {
    let mut hotels: HashMap<String, HotelRecord> = HashMap::new();

    let file = File::open(path)?;
    let reader = BufReader::new(file);

    for line in reader.lines() {
        let hotel_record: HotelRecord = serde_json::from_str(line?.as_str())?;
        hotels.insert(hotel_record.id.clone(), hotel_record);
    }

    Ok(hotels)
}

fn read_rooms(path: &str) -> Result<RoomStorage, Error> {
    let mut reader = csv::ReaderBuilder::new()
        .delimiter(b'|')
        .has_headers(false)
        .from_path(path)?;
    let mut rooms: RoomStorage = HashMap::new();

    for result in reader.deserialize::<RoomRecord>() {
        let record = result?;
        let room_key = RoomHashKey {
            hotel_code: record.hotel_code,
            source: record.source,
            room_code: record.room_code,
        };
        rooms.insert(room_key, record.room_name);
    }

    Ok(rooms)
}

fn main() {
    if let Err(e) = process_input("./input/") {
        eprintln!("{}", e);
    }
}
