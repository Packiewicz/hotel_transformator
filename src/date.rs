// VEEEERY simple date handling lib

static DAYS_IN_MONTH: [i32; 12] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

#[derive(Clone)]
pub struct Date {
    year: i32,
    month: i32,
    day: i32,
}

impl Date {
    /*
    Expects date in format YYYYMMDD, e.x. 20180721
     */
    pub fn from_str(s: &str) -> Option<Date> {

        if s.chars().count() < 8 {
            return None;
        }

        let mut date_numbers: [i32; 3] = [0, 0, 0];
        date_numbers[0] = match s[0..4].parse() {
            Ok(o) => o,
            _ => return None,
        };
        date_numbers[1] = match s[4..6].parse() {
            Ok(o) => o,
            _ => return None,
        };
        date_numbers[2] = match s[6..8].parse() {
            Ok(o) => o,
            _ => return None,
        };

        let date = Date {
            year: date_numbers[0],
            month: date_numbers[1],
            day: date_numbers[2],
        };

        if date.verify() {
            Some(date)
        } else {
            None
        }
    }

    /*
    Assumes dates fitting in UNIX timestamp
     */
    fn verify(&self) -> bool {
        if self.day <= 0 || self.day > 31 {
            return false;
        }
        if self.month <= 0 || self.month > 12 {
            return false;
        }
        if self.year <= 1970 {
            return false;
        }
        true
    }

    pub fn add_day(&mut self) {
        self.day += 1;
        if self.day > DAYS_IN_MONTH[self.month as usize] {
            self.day = 1;
            self.month += 1;
            if self.month > 12 {
                self.month = 1;
                self.year += 1;                
            }
        }
    }

    pub fn to_string(&self) -> String {
        format!("{:04}-{:02}-{:02}", self.year, self.month, self.day) 
    }
}

#[cfg(test)]
mod date_tests {
    use crate::date::Date;

    #[test]
    fn test_parse() {
        let test_string = "20180721";
        let date = Date::from_str(test_string);
        
        assert!(date.is_some());

        let date = date.unwrap();

        assert_eq!(date.year, 2018);
        assert_eq!(date.month, 7);
        assert_eq!(date.day, 21);
    }
}